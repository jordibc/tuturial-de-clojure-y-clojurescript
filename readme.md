# Tutorial de Clojure/ClojureScript

## Intro

Clojure es una pasada. Es, en cierto modo, el lenguaje que siempre he
querido tener. (¿Por qué? Lo cuento en [mis razones](razones.md).)

Estas notas son sobre todo, más que para hacer un tutorial de Clojure,
para contar lo que me ha resultado más relevante para aprenderlo, con
enlaces a los mejores recursos que conozco. ¡Y que sea más fácil
empezar y divertirse!


## Instalación

Muchas distribuciones de GNU/Linux tienen un paquete para instalar
Clojure (`apt get install clojure` para Debian, por ejemplo). Pero
aunque normalmente sea mejor instalar software así, el paquete de
Debian tiene algunos inconvenientes, como ir alguna versión por
detrás, y no contener el comando `clj`. Es mejor instalarlo con el
script en la web de Clojure:
https://clojure.org/guides/install_clojure

En mi caso, lo instalo en `~/.local` (más sobre eso en la sección de
[directorios](directorios.md)) con:

```sh
./linux-install.sh --prefix $HOME/.local
```


## Primeras pruebas

Después de instalar Clojure puedes escribir en un terminal:

```sh
clj
```

Esto te hará entrar en la repl ("Read Eval Print Loop"), donde ya
puedes empezar a jugar con todas las expresiones del lenguaje.

Puedes probar cosas como:

```clojure
"Hola mundo"
;; Esto es un comentario, y la línea anterior un programa "hello world" completo.
(+ 1 2)  ; que va a ser 3
'(+ 1 2)  ; que no va a ser 3
(println "Hola" "y adiós")
[1 2 3]  ; vector
(doc cycle)
(take 10 (cycle [1 2 3]))
{:name "Reset", :langs ["spanish" "english" "esperanto" "tokipona"], :quiz-wins 1}
```

Además de probar los ejemplos que aparecen en las webs de la sección
de enlaces, también puedes probar esta web: https://tryclojure.org/

A otro nivel más avanzado, puedes ver estas notas para [empezar un
proyecto en ClojureScript](clojurescript.md).


## Enlaces

Hay mucho material sobre Clojure en la web. Para evitar seguir alguno
de los tutoriales que me han llevado por caminos menos directos, esta
es la lista de lo que me ha sido más útil:

* Webs
  * [Learn Clojure](https://clojure.org/guides/learn/clojure) (de la sección [Getting Started](https://clojure.org/guides/getting_started) de la web de Clojure, donde también hay muchos enlaces útiles)
  * [Página de Wikipedia](https://en.wikipedia.org/wiki/Clojure)
* Vídeos
  * [Simple Made Easy](https://www.youtube.com/watch?v=LKtk3HCgTa8)
  * [Hammock Driven Development](https://www.youtube.com/watch?v=f84n5oFoZBc)
  * [The Value of Values](https://www.youtube.com/watch?v=-6BsiVyC1kM)

Algo más avanzados, pero que también me han aclarado cosas que me
parecen importantes:

* Vídeos
  * [Maybe Not](https://www.youtube.com/watch?v=YR5WdGrpoug) (en general muchos de los del canal [ClojureTV](https://www.youtube.com/user/ClojureTV/videos?view=0&sort=p&flow=grid), los de Rich Hickey, y no los de los otros)
* Papers
  * [A history of Clojure](https://dl.acm.org/doi/10.1145/3386321)


## Qué mirar y qué evitar

Una de las cosas que me paraba más para aprender Clojure era decidirme
sobre qué probar entre las distintas alternativas que hay en varios
casos. Ahora que he probado y aprendido sobre varias, tengo bastante
claras las siguientes preferencias:

* [deps.edn](https://clojure.org/reference/deps_and_cli) (y no leiningen ni boot)
* [shadow-cljs](https://github.com/thheller/shadow-cljs) (y no figwheel)
* [reagent](https://reagent-project.github.io/) y
  [re-frame](https://github.com/day8/re-frame) (y no
  [Om](https://github.com/omcljs/om))

No vale la pena mirarlas ahora si no te suenan, pero te puede ayudar
si llegado el momento te encuentras con esas opciones y te confunden
como me confundían a mí.


## Extras

### Una readline más potente - Rebel Readline

La forma normal de ejecutar la repl de Clojure en un terminal es con
el comando `clj`. Como me gusta mucho más el entorno que ofrece [rebel
readline](https://github.com/bhauman/rebel-readline/tree/master/rebel-readline),
lo que hago es ejecutar `cl`, un pequeño script que tengo en
`$HOME/.local/bin/cl` y que es simplemente:

```sh
#!/bin/sh

clojure -Sdeps "{:deps {com.bhauman/rebel-readline {:mvn/version \"0.1.4\"}}}" \
        -M -m rebel-readline.main
```

La diferencia es parecida a ejecutar `ipython` en lugar de `python`.


### Shell scripting - Babashka

Para usar Clojure como lenguaje de scripting, el tiempo de arranque lo
hace demasiado lento.

En su lugar, [babashka](https://github.com/babashka/babashka) es un
intérprete que se lanza muy rápido y permite "aprovechar Clojure en
sitios donde estarías usando bash".


### Parseando json y similares - Jet

Para parsear ficheros de json, que es algo que me encuentro haciendo a
menudo, un programa muy bueno y popular es
[jq](https://stedolan.github.io/jq/). Aunque uno de los inconvenientes
que tiene es tener que aprender un
[dsl](https://en.wikipedia.org/wiki/Domain-specific_language) más.

Ese tipo de tareas las hace muy bien
[jet](https://github.com/borkdude/jet), que permite usar otros
formatos además de json, y hacerlo con la potencia de los comandos de
Clojure (y evitando tener que aprender otro dsl).
