# ¿Por qué Clojure?

Lo esencial para mí es que es un Lisp moderno diseñado para
programación funcional. Y es muy *expresivo* (más sobre eso luego).

Ese resumen toca temas que personalmente me interesan mucho y me han
llevado muchos años entender. Escribo esto en plan "el texto que me
hubiera gustado leer", y tiene un poco de historietas personales :)

De los lenguajes de programación que he ido conociendo, varios han
sido especiales. Más allá de algunos como Logo, Basic, C, ensamblador,
C++, y Python, con los que pensé que "sabía programar" y hasta que
podía ser un "buen programador", hay otros como Prolog, Lisp, y
Haskell que me llamaron mucho la atención y me hicieron pararme a
pensar.

Estos últimos me descubrieron conceptos que al
principio me resultaron extraños y difíciles, y luego muy chulos y
elegantes: "macros", "metaprogramación", "funcional puro", "evaluación
perezosa" (*lazy eval*), o "programación lógica" entre otros.

¿Hay alguna forma de tener todo eso al programar? ¿Son siquiera
conceptos que tiene sentido usar juntos? Porque todos aportan, según
me parece, algo en cierto modo esencial.

A excepción de la programación lógica, Clojure los usa todos por
defecto. (También tiene un sistema de programación logica, con la
librería [core.logic](https://github.com/clojure/core.logic), pero eso
es menos relevante.)

Esa combinación hace fácil crear y componer elementos sencillos de una
forma elegante. Los mecanismos de abstracción y combinación son algo
esencial (¡como aprendí gracias a [Structure and Interpretation of
Computer
Programs](https://en.wikipedia.org/wiki/Structure_and_Interpretation_of_Computer_Programs)!). Clojure
provee unos mecanismos de abstracción que hacen del lenguaje algo tan
expresivo como no he visto antes.

A la vez, entender cómo está hecho el lenguaje, qué incluye y qué deja
fuera a propósito, me ayudó a entender otro tipo de conceptos con los
que estaba más familiarizado (pero más confundido). Por ejemplo, los
tipos (*types*), los objetos, los patrones de software (*software
patterns*), o las mónadas entre otros. Cosas que sabía usar e incluso
había usado extensamente, pero de las que no tenía una opinión clara;
que no terminaba de encajar en una visión de lo que es escribir buen
software.

Al aprender sobre Clojure entendí mejor el papel de esos otros
conceptos, y sobre todo entendí por qué no eran esenciales. A pesar de
las muchas décadas de desarrollo de lenguajes de programación, de las
modas y de las veces que llega gente con fanatismos de "esta es la
manera de hacer las cosas", opino como Sussman que [¡realmente no
sabemos cómo programar!](https://www.youtube.com/watch?v=HB5TrK7A4pI)
