# La "Java Virtual Machine"

## ¿El horror de Java?

Clojure corre en la
[JVM](https://en.wikipedia.org/wiki/Java_virtual_machine), la "Java
Virtual Machine". Al principio eso me tiraba mucho para atrás, porque
le tengo poco aprecio a todo lo que huele a Java.

Simplemente lanzar la repl ya tardaba un montón, lo que confirmaba
cosas que pensaba como *ineficiente*, *desesperantemente lento*, y
hasta *poco elegante*.

Pero resulta que la JVM en el fondo tiene poco que ver con
Java. Entender qué es, cómo se usa, y cómo la aprovecha Clojure es
algo que ha cambiado mi visión sobre esta elección.

Aquí me gustaría escribir un poco sobre lo esencial de la JVM y de
cómo la aprovecha Clojure. En cierto modo, aclarar lo suficiente como
para quitar el miedo y el rechazo que sentía mi yo anterior.


## Compilando a muchos sistemas a la vez

La JVM es una **máquina virtual**, una especie de ordenador
"implementado en software". En lugar de tener sus registros y demás
componentes en hardware, los tiene solo especificados.

La ventaja es que, si esa máquina está bien diseñada, es eficiente
traducirla a muchas otras arquitecturas (x86-amd64, arm, m1, etc.). Y
a la hora de compilar un programa, si se compila para esa máquina
virtual, ya se tiene funcionando de forma eficiente en todos esos
sitemas.

La JVM se ideó inicialmente para mejorar cierto aspecto de Java: que
funcionara inmediatamente en muchos sistemas. En lugar de compilar
Java para cada sistema, la idea es que se compilara para la JVM (se
compilara a *bytecode*). Y tener por otro lado una implementación
eficiente de la JVM en muchos sistemas.

Hoy en día la JVM es una máquina virtual madura, y esas
implementaciones eficientes existen. Pero no hay nada que impida usar
esa JVM con otras cosas que no sean Java. Cualquier cosa que compile
al mismo bytecode que entiende la JVM, se ejecutará con la misma
eficiencia.

Así que aprovechando la existencia de la JVM, han aparecido otros
lenguajes que compilan a bytecode para esa máquina virtual. Lenguajes
que no tienen nada que ver con Java, como Scala, Kotlin, o Groove. Y
Clojure.

Al hacerlo, Clojure no depende en nada de Java, ni su sintaxis se ve
afectada por Java ni nada por el estilo. Al hacerlo, además de correr
en todos los sistemas en que corre la JVM, puede aprovechar las
funcionalidades avanzadas de esa máquina virtual (como el garbage
collector), e interaccionar fácilmente con cualquier otro lenguaje que
también compile a la JVM.
