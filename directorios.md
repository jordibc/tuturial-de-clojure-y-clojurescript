# Directorios

## Mi configuración

Los directorios que uso como ejemplos son los que normalmente uso en
mi sistema.  El lugar donde instalo el software para mi usuario es en
`$HOME/.local`.

La estructura en ese directorio es similar a la de `/usr/local`, así
que por ejemplo mi `PATH` incluye `$HOME/.local/bin` y mi
`LD_LIBRARY_PATH` incluye `$PATH/.local/lib`.


## ¿Dónde guarda las librerías Clojure?

Cuando se ponen dependencias en un proyecto, las librerías que se
descargan de internet vienen de [clojars](https://clojars.org/) y de
[maven](https://maven.apache.org/), que hacen el papel equivalente al
"Python Package Index" ([PyPI](https://pypi.org/)) en el mundo Python.

Y lo que en Python iría a `lib/python$VERSION/dist-packages`, en
Clojure van a `$HOME/.m2` (por
[maven](https://maven.apache.org/settings.html)).


## ¿Dónde guarda las librerías ClojureScript?

Principalmente en `node_modules`, como hace
[npm](https://www.npmjs.com/).

En mi caso, tengo lo siguiente en el fichero de configuración
`$HOME/.nmprc`:

```ini
prefix=/home/jordi/.local
```

lo que hace que las librerías globales aparezcan en
`$HOME/.local/lib/node_modules`.

Algunas de las cosas que se instalan para usar ClojureScript en
realidad no tienen que ver con módulos en javascript, así que también
habrá algo en `$HOME/.m2`.
