# ClojureScript

## Compilando a otra plataforma

En principio, un programa en Clojure podría compilar a lo que
quisiéramos, no tiene por qué estar limitado a compilar [bytecode para
la JVM](jvm.md). Y hoy en día existe de hecho otro compilador que
convierte código de Clojure a JavaScript. El código de Clojure que
está preparado para usarse como JavaScript desde el browser (o node,
etc.) es lo que se compila con *ClojureScript*.

Aparte de Clojure (hablando del compilador que compila a la JVM) y
ClojureScript (el que compila, o "transpila", a JavaScript), existe
algún compilador más, pero ningún otro que hoy en día merezca la pena.


## Cómo empezar un proyecto de cero

Esto es lo que hago cuando empiezo un proyecto con
[ClojureScript](https://clojurescript.org/).

Primero ejecuto con `npx` el paquete `create-cljs-project` que se
bajará del repositorio de npm, diciéndole en qué directorio quiero mi
proyecto:

```sh
npx create-cljs-project myproject
cd myproject
```

Luego creo un fichero sencillo en `src/main/app.cljs` con algo así
como:

```clojure
(ns app)

(defn init []
  (println "Hello world"))
```

Entonces edito `shadow-cljs.edn` y añado:

```clojure
 :builds
 {:app
  {:target :browser
   :modules {:main {:init-fn app/init}}}}
```

En este punto es posible jugar con `npx shadow-cljs watch app` (y en
cualquier momento, con `npx shadow-cljs browser-repl`).

A la hora de seguir, me resulta más chulo hacerlo con emacs y
[cider](https://github.com/clojure-emacs/cider) (hay alternativas para
otros editores, que se pueden ver en [la página sobre
herramientas](https://clojure.org/community/tools)). Para eso,
necesitaremos tener más cosas en nuestro proyecto.

Añadiría esto a `shadow-cljs.edn`:

```clojure
 :dev-http {8080 "public"}
```

y tendría este fichero `index.html` en aquel directorio "public" donde
he dicho que debería haber un servidor http sirviendo:

```html
<!doctype html>
<html>
  <head>
    <meta charset="utf-8" />
  </head>
  <body>
    <div id="root"></div>
    <script src="/js/main.js"></script>
  </body>
</html>
```

Ahora, en emacs, ejecuto `cider-jack-in-cljs` con los parámetros
`shadow` y `app` cuando me los pregunta. Abro `localhost:8080` y el
inspector (F12). Recargo la página una vez cider dice "build
completed". Y ahora cider debería estar listo (puedes usar la REPL, o
evaluar código con `C-x C-e`).

Normalmente también uso [reagent](https://reagent-project.github.io/),
para lo que simplemente hay que añadir esto en `shadow-cljs.edn`:

```clojure
 :dependencies
 [[reagent "RELEASE"]]
```

Cuando voy a crear una versión optimizada para subir a la web, ejecuto:

```sh
npx shadow-cljs release app
```

y subo los ficheros `public/index.html` y `public/js/main.js` a una
web. Eso es todo lo que hace falta.

Mi forma de empezar a trabajar en un proyecto de ClojureScript la saco
principalmente de esta referencia:
https://github.com/thheller/shadow-cljs#quick-start


## Material UI

Para usar los componentes de [material ui](https://mui.com/), además
de reagent utilizo esta librería:
https://github.com/arttuka/reagent-material-ui

Lo único que hace falta para empezar a usarlo es añadir a
`shadow-cljs.edn`:

```clojure
:dependencies
[...
 [arttuka/reagent-material-ui "RELEASE"]
 ...]
```

y en el fichero cljs:

```clojure
(ns app
  (:require ...
            [reagent-mui.components :as mui]
            ...))
```


## Enlaces

* [Shadow CLJS User's Guide](https://shadow-cljs.github.io/docs/UsersGuide.html)
* [Traductor de javascript a clojurescript](https://roman01la.github.io/javascript-to-clojurescript/)
