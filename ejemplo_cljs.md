# Pasos de ejemplo para hacer una app con reagent y quil

```sh
npx create-cljs-project quil-test

cd quil-test
```

Ya se podría jugar un poco con:

```sh
npx shadow-cljs browser-repl
```

Después, editar `shadow-cljs.edn` y poner:

```clojure
 :dependencies
 [[reagent "RELEASE"]
  [quil "RELEASE"]]

 :builds
 {:app
  {:target :browser
   :modules {:main {:init-fn app/render-simple}}}}}
```

e incluso esto para que sirva las páginas desde el directorio `public`:

```clojure
 :dev-http
 {8080 "public"}
```

Ahora, si creamos `src/main/app.cljs` con una función `render-simple`,
ya podemos empezar la diversión.

Podemos correrlo con:

```sh
npx shadow-cljs watch app
```

que compilará la app, y creará el directorio `public`, con el js
generado. Pero todavía no mostrará nada porque no hay un `index.html`
ahí. Se puede crear así:

```html
<!doctype html>
<html>
  <head>
    <meta charset="utf-8" />
    <title>acme frontend</title>
  </head>
  <body>
    <div id="root"></div>
    <div id="canvas"></div>
    <script src="/js/main.js"></script>
  </body>
</html>
```

Podríamos ejecutarlo con `npx shadow-cljs watch app`, pero es más
divertido hacerlo con cider en emacs: `M-x cider-jack-in-clsj`
`shadow` `app`.

Con una `app.cljs` que use reagent y quil, no se generaría el código,
porque le faltan algunos paquetes. Se pueden instalar trivialmente con
npm o yarn:

```sh
yarn add react react-dom

yarn add p5
```

Ahora cider generará correctamente la app. No hace falta hacer ningún
cambio extra a `shadow-cljs.edn`, los cambios necesarios (y pequeños)
ya han ocurrido en `package.json` y shadow-cljs es suficientemente
listo como para no necesitar nada más. ¡Alucinante!


## Resumen de la forma final de los ficheros

`public/index.html`

```html
<!doctype html>
<html>
  <head>
    <meta charset="utf-8" />
    <title>acme frontend</title>
  </head>
  <body>
    <div id="root"></div>
    <div id="canvas"></div>
    <script src="/js/main.js"></script>
  </body>
</html>
```

`shadow-cljs.edn`

```clojure
;; shadow-cljs configuration
{:source-paths
 ["src/dev"
  "src/main"
  "src/test"]

 :dependencies
 [[reagent "RELEASE"]
  [quil/quil "RELEASE"]]

 :dev-http
 {8080 "public"}

 :builds
 {:app
  {:target :browser
   :modules {:main {:init-fn app/render-simple}}}}}
```


`package.json`

```json
{
  "name": "quil-test",
  "version": "0.0.1",
  "private": true,
  "devDependencies": {
    "shadow-cljs": "2.18.0"
  },
  "dependencies": {
    "p5": "^1.4.1",
    "quil": "^1.1.30",
    "react": "^18.0.0",
    "react-dom": "^18.0.0"
  }
}
```


`src/main/app.cljs`

```clojure
(ns app
  (:require [reagent.core :as r]
            [reagent.dom :as rdom]
            [quil.core :as q :include-macros true]))

(def click-count (r/atom 0))

(defn counting-component []
  [:div "The atom " [:code "click-count"] " has value: " @click-count ". "
   [:input {:type "button" :value "Click me!" :on-click #(swap! click-count inc)}]])


(defn calc-bmi [{:keys [height weight bmi] :as data}]
  (let [h (/ height 100)]
    (if (nil? bmi)
      (assoc data :bmi (/ weight (* h h)))
      (assoc data :weight (* bmi h h)))))

(def bmi-data (r/atom (calc-bmi {:height 180 :weight 80})))

(def radius (atom 20))

(defn slider [param value min max invalidates]
  [:input
   {:type "range" :value value :min min :max max :style {:width "100%"}
    :on-change (fn [e]
                 (let [new-value (js/parseInt (.. e -target -value))]
                   (reset! radius (/ new-value 10))
                   (swap! bmi-data
                          (fn [data]
                            (-> data
                                (assoc param new-value)
                                (dissoc invalidates)
                                calc-bmi)))))}])

(defn bmi-component []
  (let [{:keys [weight height bmi]} @bmi-data
        [color diagnose] (cond
                          (< bmi 18.5) ["orange" "underweight"]
                          (< bmi 25) ["inherit" "normal"]
                          (< bmi 30) ["orange" "overweight"]
                          :else ["red" "obese"])]
    [:div
     [:h3 "BMI calculator"]
     [:div "Height: " (int height) "cm" [slider :height height 100 220 :bmi]]
     [:div "Weight: " (int weight) "kg" [slider :weight weight  30 150 :bmi]]
     [:div "BMI: "    (int bmi)    "  " [:span {:style {:color color}} diagnose]
      [slider :bmi bmi 10 50 :weight]]]))



(defn simple-component []
  [:div
   [:p "I am a component!"]
   [counting-component]
   [bmi-component]
   [:p.someclass "I have " [:strong "bold"]
    [:span {:style {:color "red"}} " and red "] "text."]])


(defn render-simple []
  (rdom/render
    [simple-component]
    (.getElementById js/document "canvas")))

(defn setup []
  (q/frame-rate 1)                    ;; Set framerate to 1 FPS
  (q/background 200))                 ;; Set the background colour to
                                      ;; a nice shade of grey.
(defn draw []
  (q/stroke (q/random 255))             ;; Set the stroke colour to a random grey
  (q/stroke-weight (q/random 10))       ;; Set the stroke thickness randomly
  (q/fill (q/random 255))               ;; Set the fill colour to a random grey

  (let [r @radius
        x (q/random (q/width))       ;; Set the x coord randomly within the sketch
        y (q/random (q/height))]     ;; Set the y coord randomly within the sketch
    (q/ellipse x y r r)))            ;; Draw a circle at x y with the correct diameter


(q/defsketch my-sketch-definition
  :host "root"
  :draw draw
  :size [500 500])
```
